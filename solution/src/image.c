#include <malloc.h>

#include "image.h"

struct image image_create(const size_t width, const size_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
}

void image_free(struct image *img) {
    free(img->data);
}

struct pixel *pixel_at(const struct image *const img, const size_t x, const size_t y) {
    return img->data + img->width * y + x;
}

bool pixel_equal(const struct pixel pixel1, const struct pixel pixel2) {
    return pixel1.r == pixel2.r
           && pixel1.b == pixel2.b
           && pixel1.g == pixel2.g;
}

bool image_equal(const struct image image1, const struct image image2) {
    if (image1.height != image2.height
        || image1.width != image2.width) {
        return false;
    }
    for (size_t i = 0; i < image1.height; i++) {
        for (size_t j = 0; j < image1.width; j++) {
            if (!pixel_equal(*pixel_at(&image1, j, i), *pixel_at(&image2, j, i))) {
                return false;
            }
        }
    }
    return true;
}


enum read_status image_read(const char *src, image_format_read *image_format_read, struct image * img) {
    FILE *file = fopen(src, "rb");
    if (!file) {
        return READ_INVALID_PATH;
    }
    enum read_status status = image_format_read(file, img);
    fclose(file);
    return status;
}

enum write_status image_write(const char *out, image_format_write *image_format_read, struct image img) {
    FILE *file = fopen(out, "wb");
    if (!file) {
        return WRITE_INVALID_PATH;
    }
    enum write_status status = image_format_read(file, img);
    fclose(file);
    return status;
}
