#include "image.h"
#include "image_transform.h"


struct image image_transform_rotate(struct image img) {
    struct image blank = image_create(img.height, img.width);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++)
            *pixel_at(&blank, j, i) = *pixel_at(&img, i, img.height - 1 - j);
    }
    return blank;
}

struct image image_transform_h_mirror(struct image img) {
    struct image blank = image_create(img.width, img.height);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++)
            *pixel_at(&blank, j, i) = *pixel_at(&img, img.width - j, i);
    };
    return blank;
}

struct image image_transform_v_mirror(struct image img) {
    struct image blank = image_create(img.width, img.height);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++)
            *pixel_at(&blank, j, i) = *pixel_at(&img, j, img.height - i - 1);
    }
    return blank;
}


struct image image_transform_color(struct image img, struct pixel (pixel_colorer)(struct pixel)) {
    struct image blank = image_create(img.width, img.height);

    for (size_t i = 0; i < blank.height; i++) {
        for (size_t j = 0; j < blank.width; j++) {
            *pixel_at(&blank, j, i) = pixel_colorer(*pixel_at(&img, j, i));
        }
    }
    return blank;
}

struct pixel pixel_color_invert(struct pixel pixel) {
    return (struct pixel) {
            .g = 255-pixel.g,
            .r = 255-pixel.r,
            .b = 255-pixel.b
    };
}

struct image image_transform_color_invert(struct image img) {
    return image_transform_color(img, pixel_color_invert);
}

static struct pixel pixel_color_bw(struct pixel pixel) {
    uint8_t mean = (pixel.b+pixel.g+pixel.r)/3;
    return (struct pixel) {
            .g = mean,
            .r = mean,
            .b = mean
    };
}

struct image image_transform_color_bw(struct image img) {
    return image_transform_color(img, pixel_color_bw);
}


static struct pixel pixel_color_sepia(struct pixel pixel) {
    double dr = pixel.r, dg = pixel.g, db = pixel.b;
    double tr = 0.393*dr + 0.769*dg + 0.189*db;
    double tg = 0.349*dr + 0.686*dg + 0.168*db;
    double tb = 0.272*dr + 0.534*dg + 0.131*db;
    if(tr>255) tr = 255;
    if(tg>255) tg = 255;
    if(tb>255) tb = 255;
    return (struct pixel) {
            .b = (uint8_t) tb,
            .g = (uint16_t) tg,
            .r = (uint8_t) tr
    };
}


struct image image_transform_color_sepia(struct image img) {
    return image_transform_color(img, pixel_color_sepia);
}

struct image image_transform_color_f_sepia(struct image img) {
    // TODO: ASM implementation

    return image_transform_color_sepia(img);
}


bool image_transform_apply(image_transform effect, struct image_format image_format, const char* src, const char* out) {
    struct image img;

    enum read_status read_status = image_read(src, image_format.read, &img);
    if (read_status != READ_OK) {
        return false;
    }
    struct image transformed_image = effect(img);
    image_free(&img);

    enum write_status write_status = image_write(out, image_format.write, transformed_image);
    image_free(&transformed_image);

    if (write_status != WRITE_OK) {
        return false;
    }
    return true;
}

