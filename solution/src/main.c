#include "image_bmp.h"
#include "image_transform.h"


int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    const char *in = argv[1];
    const char *out = argv[2];

    image_transform *rotate = image_transform_rotate;

    struct image_format image_bmp = {
        .read = image_bmp_read,
        .write = image_bmp_write
    };


    return !image_transform_apply(rotate, image_bmp, in, out);
}
