#include "image.h"
#include "image_bmp.h"

#define BMP_HEADER_TYPE 0x4d42
#define BMP_HEADER_RESERVED 0
#define BMP_HEADER_SIZE 40
#define BMP_HEADER_PLANES 1
#define BMP_HEADER_BIT_COUNT sizeof(struct pixel) * 8
#define BMP_HEADER_COMPRESSION 0
#define BMP_HEADER_X_PELS_PER_METER 0
#define BMP_HEADER_Y_PELS_PER_METER 0
#define BMP_HEADER_CLR_USED 0
#define BMP_HEADER_CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t bmp_padding(uint64_t width) {
    uint8_t ost = width * sizeof(struct pixel) % 4;
    if (ost == 0) return ost;
    else return 4 - ost;
}

static uint32_t bmp_size(const struct image* image) {
    return image->height * (image->width * sizeof(struct pixel) + bmp_padding(image->width));
}


static struct bmp_header bmp_header_create(const struct image* bmp) {
    return (struct bmp_header) {
            .bfType              = BMP_HEADER_TYPE,
            .bfileSize           = bmp_size(bmp) + sizeof(struct bmp_header),
            .bfReserved          = BMP_HEADER_RESERVED,
            .bOffBits            = sizeof(struct bmp_header),
            .biSize              = BMP_HEADER_SIZE,
            .biWidth             = bmp->width,
            .biHeight            = bmp->height,
            .biPlanes            = BMP_HEADER_PLANES,
            .biBitCount          = BMP_HEADER_BIT_COUNT,
            .biCompression       = BMP_HEADER_COMPRESSION,
            .biSizeImage         = bmp_size(bmp),
            .biXPelsPerMeter     = BMP_HEADER_X_PELS_PER_METER,
            .biYPelsPerMeter     = BMP_HEADER_Y_PELS_PER_METER,
            .biClrUsed           = BMP_HEADER_CLR_USED,
            .biClrImportant      = BMP_HEADER_CLR_IMPORTANT
    };
}

static enum read_status bmp_header_read(FILE* file, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
        return READ_INVALID_HEADER;
    } else {
        return READ_OK;
    }
}

static enum read_status bmp_body_read(FILE* file, struct image* bmp) {
    for (size_t i = 0; i < bmp->height; i++) {
        if (fread(bmp->data + bmp->width * i, sizeof(struct pixel),bmp->width, file) != bmp->width) {
            return READ_INVALID_BITS;
        }
        if (fseek(file, bmp_padding(bmp->width), SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum read_status image_bmp_read(FILE* file, struct image *bmp) {
    if(file == NULL) return READ_INVALID_PATH;

    struct bmp_header header;
    enum read_status header_status = bmp_header_read(file, &header);
    if(header_status!=READ_OK) return header_status;

    if (fseek(file, (long) header.bOffBits, SEEK_SET) != 0) {
        return READ_INVALID_SIGNATURE;
    }

    *bmp = image_create(header.biWidth, header.biHeight);
    return bmp_body_read(file, bmp);
}

static enum write_status bmp_header_write(FILE* out, struct bmp_header header) {
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    if(ferror(out) !=0) {
        return WRITE_HEADER_ERROR;
    }
    return WRITE_OK;
}

static enum write_status bmp_body_write(FILE* out, struct image bmp) {
    for (uint64_t i = 0; i < bmp.height; i++) {
        fwrite(bmp.data + bmp.width * i, sizeof(struct pixel), bmp.width,out);
        if(fseek(out, bmp_padding(bmp.width), SEEK_CUR) != 0) return WRITE_INVALID_DATA;
    }
    return WRITE_OK;
}

enum write_status image_bmp_write(FILE* out, struct image bmp) {
    if(!out) return WRITE_INVALID_PATH;

    const struct bmp_header header = bmp_header_create(&bmp);
    enum write_status header_status = bmp_header_write(out, header);
    if(header_status!=WRITE_OK) return header_status;

    return bmp_body_write(out, bmp);
}
