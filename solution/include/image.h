#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct pixel { uint8_t b, g, r; };

struct image {
    size_t width, height;
    struct pixel* data;
};

// Return new image with required width and height
struct image image_create(size_t width, size_t height);

// Free memory of image's data
void image_free(struct image* img);

// Return pointer to pixel_at by coordinates
struct pixel* pixel_at(const struct image *img, size_t x, size_t y);

// Pixels are equal
bool pixel_equal(struct pixel pixel1, struct pixel pixel2);

// Images are equal
bool image_equal(struct image image1, struct image image2);

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PATH
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_INVALID_PATH,
    WRITE_INVALID_DATA
};

// Method to read image of some format
typedef enum read_status (image_format_read)(FILE *, struct image*);

// Method to write image of some format
typedef enum write_status (image_format_write)(FILE *, struct image);

// Store methods to read from some format and write to some format
struct image_format {
    image_format_read *read;
    image_format_write *write;
};

// Read and store image from file by path, using method based by format
enum read_status image_read(const char* src, image_format_read *image_format_read, struct image* img);

// Write image form file by path, using method based by format
enum write_status image_write(const char* out, image_format_write *image_format_read, struct image img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
