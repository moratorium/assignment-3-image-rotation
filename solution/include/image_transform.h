#ifndef IMAGE_TRANSFORMER_FACTORY_H
#define IMAGE_TRANSFORMER_FACTORY_H

#include <stdbool.h>
#include <stdio.h>

#include "image.h"


// Type of image transformers
typedef struct image (image_transform)(struct image source);

// Rotate image by 90 degrees
image_transform image_transform_rotate;

// Mirror the image by horizontal axis
image_transform image_transform_h_mirror;

// Mirror the image by vertical axis
image_transform image_transform_v_mirror;

// Invert the color of image
image_transform image_transform_color_invert;

// Black-and-white filter
image_transform image_transform_color_bw;

// Sepia filter
image_transform image_transform_color_sepia;

// Fast sepia filter
image_transform image_transform_color_f_sepia;

// Apply effect to image file
bool image_transform_apply(image_transform effect, struct image_format image_format, const char* src, const char* out);

#endif //IMAGE_TRANSFORMER_FACTORY_H
